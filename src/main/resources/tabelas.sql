CREATE TABLE usuario (
	codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	nome VARCHAR(50) NOT NULL,
	email VARCHAR(30) NOT NULL unique,
	telefone VARCHAR(30) NOT NULL,
	senha VARCHAR(30),
	ativo boolean NOT NULL
)

CREATE TABLE mensagemconfirmacao (
	codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	usuario BIGINT(20) NOT NULL,
	operacao BIGINT(20) NOT NULL,
	pin varchar(20) NOT NULL,
	confirmada boolean NOT NULL,
	data timestamp not null,
	foreign key (usuario) references usuario(codigo)
)

