package br.com.certificart.exception;

import org.springframework.http.HttpStatus;

import br.com.certificart.resource.ErroResource;

public class ApiException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/** Codigo de erro HTTP */
	private HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;

	public ApiException() {
		super();
	}

	public ApiException(final String message, final Throwable cause, final boolean enableSuppression,
			final boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ApiException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public ApiException(final String message) {
		super(message);
	}

	public ApiException(final Throwable cause) {
		super(cause);
	}

	public HttpStatus getStatus() {
		return this.status;
	}

	public void setStatus(final HttpStatus status) {
		this.status = status;
	}

	public ApiException(final HttpStatus status, final String message) {
		super(message);
		this.status = status;
	}

	public ErroResource getErro() {
		return new ErroResource(this.status, this.getMessage(), this);
	}
}
