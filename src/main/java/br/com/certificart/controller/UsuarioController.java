package br.com.certificart.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.certificart.exception.ApiException;
import br.com.certificart.model.Usuario;
import br.com.certificart.resource.UsuarioResource;
import br.com.certificart.service.UsuarioService;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;

	@GetMapping("/{codigo}")
	public ResponseEntity<Object> obterUsuarioPorCodigo(@PathVariable final Long codigo) {
		try {
			final Usuario usuario = this.usuarioService.buscarPorCodigo(codigo);
			return ResponseEntity.status(HttpStatus.OK).body(usuario);
		} catch (final ApiException e) {
			return ResponseEntity.status(e.getStatus()).body(e.getErro());
		}
	}

	@GetMapping
	public ResponseEntity<Object> obterUsuarios() {
		try {
			final List<UsuarioResource> usuarios = this.usuarioService.buscar();
			return ResponseEntity.status(HttpStatus.OK).body(usuarios);
		} catch (final ApiException e) {
			return ResponseEntity.status(e.getStatus()).body(e.getErro());
		}
	}

	@PostMapping
	public ResponseEntity<Object> inserirUsuario(@Valid @RequestBody final Usuario usuario) {
		try {
			final UsuarioResource usuarioGravado = this.usuarioService.inserirUsuario(usuario);
			return ResponseEntity.status(HttpStatus.OK).body(usuarioGravado);
		} catch (final ApiException e) {
			return ResponseEntity.status(e.getStatus()).body(e.getErro());
		}
	}

	@DeleteMapping("/{codigo}")
	public ResponseEntity<Object> remover(@PathVariable final Long codigo) {
		try {
			this.usuarioService.deletar(codigo);
			return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
		} catch (final ApiException e) {
			return ResponseEntity.status(e.getStatus()).body(e.getErro());
		}
	}

	@PutMapping("/{codigo}")
	public ResponseEntity<Object> atualizar(@PathVariable final Long codigo,
			@Valid @RequestBody final Usuario usuario) {
		try {
			usuario.setCodigo(codigo);
			final UsuarioResource usuarioSalvo = this.usuarioService.atualizar(usuario);
			return ResponseEntity.ok(usuarioSalvo);
		} catch (final ApiException e) {
			return ResponseEntity.status(e.getStatus()).body(e.getErro());
		}
	}

}
