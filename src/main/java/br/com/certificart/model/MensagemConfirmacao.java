package br.com.certificart.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "mensagemconfirmacao")
public class MensagemConfirmacao {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;

	@Column(name = "pin")
	private String pin;

	@Column(name = "data")
	private LocalDateTime data;

	@Column(name = "operacao")
	private Long operacao;

	@Column(name = "confirmada")
	private boolean confirmada;

	@ManyToOne
	@JoinColumn(name = "usuario", nullable = false)
	private Usuario usuario;

	public Long getCodigo() {
		return this.codigo;
	}

	public void setCodigo(final Long codigo) {
		this.codigo = codigo;
	}

	public String getPin() {
		return this.pin;
	}

	public void setPin(final String pin) {
		this.pin = pin;
	}

	public LocalDateTime getData() {
		return this.data;
	}

	public void setData(final LocalDateTime data) {
		this.data = data;
	}

	public Long getOperacao() {
		return this.operacao;
	}

	public void setOperacao(final Long operacao) {
		this.operacao = operacao;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(final Usuario usuario) {
		this.usuario = usuario;
	}

	public boolean isConfirmada() {
		return confirmada;
	}

	public void setConfirmada(boolean confirmada) {
		this.confirmada = confirmada;
	}

}
