package br.com.certificart.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "usuario")
public class Usuario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;

	@Column(name = "nome")
	private String nome;

	@Column(name = "email", unique = true)
	private String email;

	@Column(name = "telefone")
	private String telefone;

	@Column(name = "senha")
	private String senha;

	@Column(name = "ativo")
	private boolean ativo;

	@Column(name = "confirmado")
	private boolean confirmado;

	public Long getCodigo() {
		return this.codigo;
	}

	public void setCodigo(final Long codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(final String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getSenha() {
		return this.senha;
	}

	public void setSenha(final String senha) {
		this.senha = senha;
	}

	public String getTelefone() {
		return this.telefone;
	}

	public void setTelefone(final String telefone) {
		this.telefone = telefone;
	}

	public boolean isAtivo() {
		return this.ativo;
	}

	public void setAtivo(final boolean ativo) {
		this.ativo = ativo;
	}

	public boolean isConfirmado() {
		return this.confirmado;
	}

	public void setConfirmado(final boolean confirmado) {
		this.confirmado = confirmado;
	}

}
