package br.com.certificart.service;

import org.springframework.stereotype.Service;

import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

@Service
public class SMSService {

	public Message enviarMensagem(final String telefone, final String mensagem) {
		// Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

		final Message sms = Message.creator(
				new PhoneNumber(telefone),
				new PhoneNumber(telefone),
				mensagem).create();

		return sms;
	}

}
