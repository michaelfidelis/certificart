package br.com.certificart.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import br.com.certificart.exception.ApiException;
import br.com.certificart.model.Usuario;
import br.com.certificart.repository.UsuarioRepository;
import br.com.certificart.resource.UsuarioResource;

@Service
public class UsuarioService {

	private static Logger LOG = Logger.getLogger(UsuarioService.class);

	@Autowired
	private UsuarioRepository usuarioRepository;

	public Usuario buscarPorCodigo(final Long codigo) {
		try {
			LOG.info("[buscarPorCodigo()] Buscando usuario pelo codigo " + codigo);
			final Usuario usuarioSalvo = this.usuarioRepository.findOne(codigo);
			if (usuarioSalvo == null) {
				throw new EmptyResultDataAccessException(1);
			}
			return usuarioSalvo;
		} catch (final DataAccessException e) {
			throw new ApiException("Erro ao buscar o usuario.", e);
		}
	}

	public Usuario buscarPorEmail(final String email) {
		try {
			LOG.info("[buscarPorCodigo()] Buscando usuario por email: " + email);
			final Usuario usuarioSalvo = this.usuarioRepository.findUsuarioByEmail(email);
			if (usuarioSalvo == null) {
				throw new ApiException(HttpStatus.NOT_FOUND, "Usuario nao encontrado.");
			}
			return usuarioSalvo;
		} catch (final DataAccessException e) {
			throw new ApiException("Erro ao buscar o usuario por email.", e);
		}
	}

	public List<UsuarioResource> buscar() {
		try {
			final List<Usuario> usuarios = this.usuarioRepository.findAll();
			if (usuarios == null) {
				throw new EmptyResultDataAccessException(1);
			}

			final List<UsuarioResource> resources = new ArrayList<>();

			for (final Usuario usuario : usuarios) {
				final UsuarioResource usuarioResource = new UsuarioResource();
				BeanUtils.copyProperties(usuario, usuarioResource, "senha");
				resources.add(usuarioResource);
			}

			return resources;
		} catch (final DataAccessException e) {
			throw new ApiException("Erro ao buscar usuarios.", e);
		}
	}

	public UsuarioResource inserirUsuario(final Usuario usuario) {
		try {
			final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			final String senhaEncriptada = passwordEncoder.encode(usuario.getSenha());

			usuario.setSenha(senhaEncriptada);
			final Usuario usuarioSalvo = this.usuarioRepository.save(usuario);

			final UsuarioResource usuarioResource = new UsuarioResource();
			BeanUtils.copyProperties(usuarioSalvo, usuarioResource, "senha");

			return usuarioResource;
		} catch (final DataAccessException e) {
			throw new ApiException("Erro ao inserir usuario.", e);
		}
	}

	public void deletar(final Long codigo) {
		try {
			this.usuarioRepository.delete(codigo);
		} catch (final DataAccessException e) {
			throw new ApiException("Erro ao remover usuario.", e);
		}
	}

	public UsuarioResource atualizar(final Usuario usuario) {
		try {
			final Usuario usuarioEncontrado = this.buscarPorCodigo(usuario.getCodigo());

			if (usuarioEncontrado == null) {
				throw new ApiException(HttpStatus.NOT_FOUND, "Usuario nao encontrado.");
			}

			if (!StringUtils.isEmpty(usuario.getNome())) {
				usuarioEncontrado.setNome(usuario.getNome());
			} else {
				usuarioEncontrado.setEmail(usuario.getEmail());
			}
			final Usuario usuarioAtualizado = this.usuarioRepository.save(usuarioEncontrado);

			final UsuarioResource usuarioResource = new UsuarioResource();
			BeanUtils.copyProperties(usuarioAtualizado, usuarioResource, "senha");

			return usuarioResource;
		} catch (final DataAccessException e) {
			throw new ApiException("Erro ao atualizar usuario.", e);
		}
	}

	public UsuarioResource confirmar(final Long codigo) {
		try {
			final Usuario usuarioEncontrado = this.buscarPorCodigo(codigo);

			if (usuarioEncontrado == null) {
				throw new ApiException(HttpStatus.NOT_FOUND, "Usuario nao encontrado.");
			}

			final Usuario usuarioAtualizado = this.usuarioRepository.save(usuarioEncontrado);

			final UsuarioResource usuarioResource = new UsuarioResource();
			BeanUtils.copyProperties(usuarioAtualizado, usuarioResource, "senha");

			return usuarioResource;
		} catch (final DataAccessException e) {
			throw new ApiException("Erro ao atualizar usuario.", e);
		}
	}

}
