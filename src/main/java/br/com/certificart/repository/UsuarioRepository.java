package br.com.certificart.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.certificart.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

	Usuario findUsuarioByEmail(String email);
}
