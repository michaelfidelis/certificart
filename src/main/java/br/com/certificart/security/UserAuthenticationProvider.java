package br.com.certificart.security;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import br.com.certificart.model.Usuario;
import br.com.certificart.repository.UsuarioRepository;

@Component
public class UserAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Override
	public Authentication authenticate(final Authentication auth)
			throws AuthenticationException {

		final String email = auth.getName();
		final String senha = auth.getCredentials().toString();

		if (StringUtils.isEmpty(email) || StringUtils.isEmpty(senha)) {
			throw new BadCredentialsException("Email e/ou senha nao informados.");
		}

		final Usuario usuarioEncontrado = this.usuarioRepository.findUsuarioByEmail(email);

		if (usuarioEncontrado == null) {
			throw new BadCredentialsException("Usuario nao encontrado.");
		} else if (!usuarioEncontrado.isAtivo()) {
			throw new BadCredentialsException("Usuario inativo");
		} else if (!usuarioEncontrado.isConfirmado()) {
			throw new BadCredentialsException("Usuario nao confirmado.");
		}

		final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

		if (passwordEncoder.matches(senha, usuarioEncontrado.getSenha())) {
			return new UsernamePasswordAuthenticationToken(email, senha, Collections.emptyList());
		} else {
			throw new BadCredentialsException("External system authentication failed");
		}
	}

	@Override
	public boolean supports(final Class<?> auth) {
		return auth.equals(UsernamePasswordAuthenticationToken.class);
	}
}