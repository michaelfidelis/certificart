package br.com.certificart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class CertificartApplication {

	public static void main(final String[] args) {
		SpringApplication.run(CertificartApplication.class, args);
	}
}
