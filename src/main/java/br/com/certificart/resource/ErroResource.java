package br.com.certificart.resource;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ErroResource {

	private HttpStatus status;
	private String mensagem;
	private String mensagemDebug;
	private List<String> detalhes;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
	private final LocalDateTime timestamp;

	public ErroResource() {
		this.timestamp = LocalDateTime.now();
	}

	public ErroResource(final HttpStatus status, final Throwable ex) {
		this();
		this.status = status;
		this.mensagem = "Unexpected error";
		this.mensagemDebug = ex.getLocalizedMessage();
	}

	public ErroResource(final HttpStatus status, final String mensagem, final Throwable ex) {
		this();
		this.status = status;
		this.mensagem = mensagem;
		this.mensagemDebug = ex.getLocalizedMessage();
	}

	public ErroResource(final HttpStatus status, final String mensagem, final String detalhe) {
		this();
		this.status = status;
		this.mensagem = mensagem;
		this.detalhes = Arrays.asList(detalhe);
	}

	public ErroResource(final HttpStatus status, final String mensagem, final List<String> detalhes) {
		this();
		this.status = status;
		this.mensagem = mensagem;
		this.detalhes = detalhes;
	}

	public HttpStatus getStatus() {
		return this.status;
	}

	public String getMensagem() {
		return this.mensagem;
	}

	public String getMensagemDebug() {
		return this.mensagemDebug;
	}

	public LocalDateTime getTimestamp() {
		return this.timestamp;
	}

	public List<String> getDetalhes() {
		return this.detalhes;
	}

	public void setDetalhes(final List<String> detalhes) {
		this.detalhes = detalhes;
	}

}
