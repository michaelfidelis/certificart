package br.com.certificart.resource;

import java.io.Serializable;

public class UsuarioResource implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long codigo;

	private String nome;

	private String email;

	private String telefone;

	private String senha;

	private boolean ativo;

	public Long getCodigo() {
		return this.codigo;
	}

	public void setCodigo(final Long codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(final String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getSenha() {
		return this.senha;
	}

	public void setSenha(final String senha) {
		this.senha = senha;
	}

	public boolean isAtivo() {
		return this.ativo;
	}

	public void setAtivo(final boolean ativo) {
		this.ativo = ativo;
	}

	public String getTelefone() {
		return this.telefone;
	}

	public void setTelefone(final String telefone) {
		this.telefone = telefone;
	}

}
