package br.com.certificart.resource;

import java.util.List;

public class CredencialResource {

	private String usuario;
	private String senha;
	private List<DominioResource<Integer>> permissoes;

	public String getUsuario() {
		return this.usuario;
	}

	public void setUsuario(final String usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return this.senha;
	}

	public void setSenha(final String senha) {
		this.senha = senha;
	}

	public List<DominioResource<Integer>> getPermissoes() {
		return this.permissoes;
	}

	public void setPermissoes(final List<DominioResource<Integer>> permissoes) {
		this.permissoes = permissoes;
	}

}
