package br.com.certificart.resource;

public class DominioResource<T> {

	private T codigo;
	private String descricao;

	public DominioResource() {
		super();
	}

	public DominioResource(final T codigo, final String descricao) {
		super();
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public T getCodigo() {
		return this.codigo;
	}

	public void setCodigo(final T codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(final String descricao) {
		this.descricao = descricao;
	}

}
